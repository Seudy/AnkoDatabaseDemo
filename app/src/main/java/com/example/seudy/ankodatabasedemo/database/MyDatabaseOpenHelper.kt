package com.example.seudy.ankodatabasedemo.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.seudy.ankolayoutdemo.model.Employee
import org.jetbrains.anko.db.*

/**
 * Created by SAMBO on 21-Jun-17.
 */
class MyDatabaseOpenHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, DB_NAME, null, 1) {
    override fun onCreate(db: SQLiteDatabase) {
        db.createTable("Employee", true,
                "id" to INTEGER + PRIMARY_KEY + UNIQUE,
                "name" to TEXT,
                "gender" to TEXT,
                "depart" to TEXT,
                "position" to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable("Employee", true)
        onCreate(db)
    }

    companion object {
        val DB_NAME = "MYDB"
        private var instace: MyDatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): MyDatabaseOpenHelper {
            if (instace == null) instace = MyDatabaseOpenHelper(ctx.applicationContext)
            return instace!!
        }
    }

    fun insertEmployee(emp: Employee): Boolean {
        var result = this.use {
            insert("Employee",
                    "name" to emp.name,
                    "gender" to emp.gender,
                    "depart" to emp.department,
                    "position" to emp.position)
        }
        if (result >= 0) return true
        return false

    }

    fun selectAllEmployee(): List<Employee>? {
        return this.use {
            select("Employee")
                    .parseList<Employee>(rowParser { id: Int, name: String, gender: String, depart: String, position: String ->
                        com.example.seudy.ankolayoutdemo.model.Employee(id, name, gender, depart, position)
                    })
        }
    }

    fun selectEmployeeById(id: Int): Employee?{
        return this.use {
            select("Employee")
                    .whereArgs("id = {empId}", "empId" to id)
                    .parseSingle<Employee>(rowParser { id: Int, name: String, gender: String, depart: String, position: String ->
                        Employee(id, name, gender, depart, position)
                    })
        }
    }

    fun updateEmployee(id: Int, name: String = "", depart: String = "", position: String = ""): Boolean {
        var values = ContentValues()
        if (name.length != 0) values.put("name", name)
        if (depart.length != 0) values.put("depart", depart)
        if (position.length != 0) values.put("position", position)
        var result = this.use {
            update("Employee",
                    values,
                    "id = ?",
                    arrayOf<String>(id.toString()))

        }
        if (result == 0) return false
        return true
    }

}