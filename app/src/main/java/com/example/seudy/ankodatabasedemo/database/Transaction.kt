package com.example.seudy.ankodatabasedemo.database

import android.content.Context
import com.example.seudy.ankolayoutdemo.model.Employee

/**
 * Created by SAMBO on 21-Jun-17.
 */

val Context.database: MyDatabaseOpenHelper
    get() = MyDatabaseOpenHelper.getInstance(applicationContext)
