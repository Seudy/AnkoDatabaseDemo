package com.example.seudy.ankodatabasedemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.seudy.ankodatabasedemo.database.database
import com.example.seudy.ankolayoutdemo.model.Employee
import org.jetbrains.anko.db.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*var arrList = listOf<Employee>(Employee(name = "John", gender = "Male", department = "Smart", position = "Team Leader"),
                Employee(name = "Mary", gender = "Female", department = "HR", position = "Manager"),
                Employee(name = "Charles", gender = "Male", department = "Marketing", position = "Team Leader"),
                Employee(name = "Kate", gender = "Female", department = "Design", position = "Manager"),
                Employee(name = "Zyn", gender = "Male", department = "OutSourcing", position = "Manager"))

        for(emp in arrList){
            if(!database.insertEmployee(emp))continue
        }*/

        // select all employees
        showAllEmployee()

        // update employee whose id = 4
        if(database.updateEmployee(id = 3, name = "Charlie Puth"))
            showEmployee(database.selectEmployeeById(4))

    }

    fun showAllEmployee(){
        var selectList = database.selectAllEmployee()
        if(selectList != null) {
            println("************* Employees ************")
            for (emp in selectList) {
                println("Empoyee ${emp.id} ${emp.name}, ${emp.gender}, ${emp.department}, ${emp.position}")
            }
        }
    }

    fun showEmployee(emp: Employee?){
        if(emp!=null) {
            println("************ ^Employee^ *************")
            println("Empoyee ${emp.id} ${emp.name}, ${emp.gender}, ${emp.department}, ${emp.position}")
        }
    }

}
